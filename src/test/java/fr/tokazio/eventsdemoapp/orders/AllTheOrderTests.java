package fr.tokazio.eventsdemoapp.orders;

import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.Address;
import fr.tokazio.eventsdemoapp.OrderLine;
import fr.tokazio.eventsdemoapp.TestBase;
import fr.tokazio.eventsdemoapp.events.OrderCreated;
import org.junit.After;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AllTheOrderTests extends TestBase {

    @After
    public void teardown() {
        Guid.reset();
    }

    @Test
    public void whenStartingShippingProcess_TheShippingShouldBeStarted() {
        Guid id = Guid.generate();
        OrderCreated orderCreated = buildOrderCreated(id, Guid.generate(), 1);
      /*  given(UseOrderCommandHandler.class, orderCreated);
        when(new StartShippingProcess(id));
        then(new ShippingProcessStarted(id));*/
    }

    @Test
    public void whenCancellingAnOrderThatHasntBeenStartedShipping_TheOrderShouldBeCancelled() {
        Guid id = Guid.generate();
        OrderCreated orderCreated = buildOrderCreated(id, Guid.generate(), 1);
     /*   given(UseOrderCommandHandler.class, orderCreated);
        when(new CancelOrder(id));
        then(new OrderCancelled(id));*/
    }

    @Test
    public void whenTryingToStartShippingACancelledOrder_IShouldBeNotified() {
        Guid id = Guid.generate();
        OrderCreated orderCreated = buildOrderCreated(id, Guid.generate(), 1);
     /*   given(UseOrderCommandHandler.class, orderCreated, new OrderCancelled(id));
        whenThrows(OrderCancelledException.class, new StartShippingProcess(id));*/
    }

    @Test
    public void whenTryingToCancelAnOrderThatIsAboutToShip_IShouldBeNotified() {
        Guid id = Guid.generate();
        OrderCreated orderCreated = buildOrderCreated(id, Guid.generate(), 1);
    /*    given(UseOrderCommandHandler.class, orderCreated, new ShippingProcessStarted(id));
        whenThrows(ShippingStartedException.class, new CancelOrder(id));*/
    }

    @Test
    public void whenShippingAnOrderThatTheShippingProcessIsStarted_ItShouldBeMarkedAsShipped() {
        Guid id = Guid.generate();
        OrderCreated orderCreated = buildOrderCreated(id, Guid.generate(), 1);
     /*   given(UseOrderCommandHandler.class, orderCreated, new ShippingProcessStarted(id));
        when(new ShipOrder(id));
        then(new OrderShipped(id));*/
    }

    @Test
    public void whenShippingAnOrderWhereShippingIsNotStarted_IShouldGetNotified() {
        Guid id = Guid.generate();
        OrderCreated orderCreated = buildOrderCreated(id, Guid.generate(), 1);
      /*  given(UseOrderCommandHandler.class, orderCreated);
        whenThrows(InvalidOrderState.class, new ShipOrder(id));*/
    }

    @Test
    public void whenTheUserCheckoutWithAnAmountLargerThan100000_TheOrderNeedsApproval() {
        Address address = new Address("Valid street");
        Guid basketId = Guid.generate();
        Guid orderId = Guid.generate();
        Guid.alwaysReturn(orderId);//Starting from here, Guid.generate() will return orderId
        OrderLine orderLine = new OrderLine(Guid.generate(), "Ball", new BigDecimal("100000"), new BigDecimal("100001"), BigDecimal.ONE);
      /*  given(UseBasketCommandHandler.class, new BasketCreated(basketId, Guid.generate(), BigDecimal.ZERO),
                new ItemAdded(basketId, orderLine),
                new BasketCheckedOut(basketId, address));
        when(new MakePayment(basketId, new BigDecimal("100001")));
        then(new OrderCreated(orderId, basketId, Arrays.asList(orderLine)), new NeedsApproval(orderId));*/
    }

    @Test
    public void whenTheUserCheckoutWithAnAmountLessThan100000_TheOrderIsAutomaticallyApproved() {
        //given
        final Address address = new Address("Valid street");
        Guid basketId = Guid.generate();
        Guid orderId = Guid.generate();
        Guid.alwaysReturn(orderId);//Starting from here, Guid.generate() will return orderId
        final OrderLine orderLine = new OrderLine(Guid.generate(), "Ball", new BigDecimal("100000"), new BigDecimal("100000"), BigDecimal.ONE);
      /*  given(UseBasketCommandHandler.class,
                new BasketCreated(basketId, Guid.generate(), BigDecimal.ZERO),
                new ItemAdded(basketId, orderLine),
                new BasketCheckedOut(basketId, address));
        //when
        when(new MakePayment(basketId, new BigDecimal("100000")));
        //then
        then(new OrderCreated(orderId, basketId, Arrays.asList(orderLine)), new OrderApproved(orderId));*/
    }

    @Test
    public void whenApprovingAnOrder_ItShouldBeApproved() {
        Guid orderId = Guid.generate();
      /*  given(UseOrderCommandHandler.class, new OrderCreated(orderId, Guid.generate(), new ArrayList<>()));
        when(new ApproveOrder(orderId));
        then(new OrderApproved(orderId));*/
    }

    private OrderCreated buildOrderCreated(Guid orderId, Guid basketId, int numberOfOrderLines) {
        return buildOrderCreated(orderId, basketId, numberOfOrderLines, new BigDecimal("100"));
    }

    private OrderCreated buildOrderCreated(Guid orderId, Guid basketId, int numberOfOrderLines, BigDecimal pricePerProduct) {
        final List<OrderLine> orderLines = new ArrayList<>();
        for (int i = 0; i < numberOfOrderLines; i++) {
            orderLines.add(new OrderLine(Guid.generate(), "Line " + i, pricePerProduct, pricePerProduct, BigDecimal.ONE));
        }
        return new OrderCreated(orderId, basketId, orderLines);
    }
}
