package fr.tokazio.eventsdemoapp.baskets;

import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.OrderLine;
import fr.tokazio.eventsdemoapp.TestBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/BasketTests/MakePaymentTests.cs
 */
@RunWith(Parameterized.class)
public class MakePaymentNotPayingTheExpectedAmountTest extends TestBase {

    @Parameterized.Parameter
    public BigDecimal productPrice;
    @Parameterized.Parameter(1)
    public BigDecimal payment;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new BigDecimal("100"), new BigDecimal("99")}, {new BigDecimal("100"), new BigDecimal("91")}, {new BigDecimal("100"), new BigDecimal("89")}
        });
    }

    @Test
    public void whenNotPayingTheExpectedAmount_IShouldGetNotified() {
        Guid id = Guid.generate();
        OrderLine existingOrderLine = new OrderLine(Guid.generate(), "", productPrice, productPrice, BigDecimal.ONE);
      /*  given(UseBasketCommandHandler.class, new BasketCreated(id, Guid.generate(), BigDecimal.ZERO),
                new ItemAdded(id, existingOrderLine));
        whenThrows(UnexpectedPaymentException.class, new MakePayment(id, payment));*/
    }
}
