package fr.tokazio.eventsdemoapp.baskets;

import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.OrderLine;
import fr.tokazio.eventsdemoapp.TestBase;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/BasketTests/MakePaymentTests.cs
 */
@RunWith(Parameterized.class)
public class MakePaymentPayingTheExpectedAmountTest extends TestBase {

    @Parameterized.Parameter
    public BigDecimal productPrice;
    @Parameterized.Parameter(1)
    public BigDecimal discountPrice;
    @Parameterized.Parameter(2)
    public BigDecimal payment;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new BigDecimal("100"), new BigDecimal("101"), new BigDecimal("101")}, {new BigDecimal("100"), new BigDecimal("80"), new BigDecimal("80")}
        });
    }

    @After
    public void tearDown() {
        Guid.reset();
    }

    @Test
    public void WhenPayingTheExpectedAmount_ThenANewOrderShouldBeCreatedFromTheResult() {
        Guid id = Guid.generate();
        Guid orderId = Guid.generate();
        Guid.alwaysReturn(orderId);
        OrderLine existingOrderLine = new OrderLine(Guid.generate(), "Ball", productPrice, discountPrice, BigDecimal.ONE);
       /* given(UseBasketCommandHandler.class, new BasketCreated(id, Guid.generate(), BigDecimal.ZERO),
                new ItemAdded(id, existingOrderLine));
        when(new MakePayment(id, payment));

        then(new OrderCreated(orderId, id, Arrays.asList(existingOrderLine)),
                new OrderApproved(orderId));*/
    }
}
