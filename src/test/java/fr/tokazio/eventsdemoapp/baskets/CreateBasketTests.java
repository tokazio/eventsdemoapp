package fr.tokazio.eventsdemoapp.baskets;

import fr.tokazio.events.CommandHandler;
import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.TestBase;
import fr.tokazio.eventsdemoapp.aggregates.Basket;
import fr.tokazio.eventsdemoapp.commandhandlers.UseBasketCommandHandler;
import fr.tokazio.eventsdemoapp.commandhandlers.UseCustomerCommandHandler;
import fr.tokazio.eventsdemoapp.commands.CreateBasket;
import fr.tokazio.eventsdemoapp.commands.CreateCustomer;
import org.junit.Test;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/BasketTests/CreateBasketTests.cs
 */
public class CreateBasketTests extends TestBase {

    @Inject
    @UseBasketCommandHandler
    private CommandHandler handler;

    @Inject
    @UseCustomerCommandHandler
    private CommandHandler customerHandler;

    @Test
    public void givenCustomerWithIdXExists_whenCreatingABasketForCustomerX_thenTheBasketShouldBeCreated() {
        //given
        Guid id = Guid.generate();
        Guid customerId = Guid.generate();
        //given
        customerHandler.execute(new CreateCustomer(customerId, "John doe"));

        //when
        final Basket basket = handler.execute(new CreateBasket(id, customerId));

        //then
        assertThat(eventStore.get(id).count()).isEqualTo(2);
        assertThat(basket.getId().toString()).isEqualTo(id.toString());
        assertThat(basket.getCustomerId().toString()).isEqualTo(customerId.toString());
        assertThat(basket.getDiscount()).isEqualByComparingTo(BigDecimal.ZERO);
        //todo verify customer name
    }

    @Test
    public void givenNoCustomerWithIdXExists_whenCreatingABasketForCustomerX_IShouldGetNotified() {
        Guid id = Guid.generate();
        Guid customerId = Guid.generate();
        //   whenThrows(AggregateNotFoundException.class, new CreateBasket(id, customerId));
    }

    @Test
    public void givenCustomerWithIdXExistsAndBasketAlreadyExistsForIdY_whenCreatingABasketForCustomerXAndIdY_IShouldGetNotified() {
        Guid id = Guid.generate();
        Guid customerId = Guid.generate();
        String name = "John doe";
      /*  given(UseBasketCommandHandler.class, new BasketCreated(id, Guid.generate(), BigDecimal.ZERO));
        given(UseCustomerCommandHandler.class, new CustomerCreated(customerId, name));
        whenThrows(BasketAlreadExistsException.class, new CreateBasket(id, customerId));*/
    }

    @Test
    public void givenACustomerWithADiscount_CreatingABasketForTheCustomer_TheDiscountShouldBeIncluded() {
        Guid id = Guid.generate();
        Guid customerId = Guid.generate();
        BigDecimal discount = new BigDecimal("89");
        String name = "John doe";
      /*  given(UseCustomerCommandHandler.class, new CustomerCreated(customerId, name),
                new CustomerMarkedAsPreferred(customerId, discount));
        when(new CreateBasket(id, customerId));
        then(new BasketCreated(id, customerId, discount));*/
    }
}
