package fr.tokazio.eventsdemoapp.baskets;

import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.OrderLine;
import fr.tokazio.eventsdemoapp.TestBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/BasketTests/AddItemToBasketTest.cs
 */
@RunWith(Parameterized.class)
public class AddItemToBasketPreferredCustomerTest extends TestBase {

    @Parameterized.Parameter
    public String productName;
    @Parameterized.Parameter(1)
    public BigDecimal itemPrice;
    @Parameterized.Parameter(2)
    public BigDecimal quantity;
    @Parameterized.Parameter(3)
    public BigDecimal discountPercentage;
    @Parameterized.Parameter(4)
    public BigDecimal discountedPrice;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"NameA", new BigDecimal("100"), new BigDecimal("10"), new BigDecimal("10"), new BigDecimal("90.0")},
                {"NameB", new BigDecimal("200"), new BigDecimal("20"), new BigDecimal("80"), new BigDecimal("40.0")}
        });
    }

    @Test
    public void givenWeHaveABasketForAPreferredCustomer_WhenAddingItems_ThePriceOfTheBasketShouldBeDiscounted() {
        //given
        final Guid customerId = Guid.generate();
        final Guid productId = Guid.generate();
        final Guid id = Guid.generate();
        final OrderLine expectedOrderLine = new OrderLine(productId, productName, itemPrice, discountedPrice, quantity);
      /*  given(UseCustomerCommandHandler.class,
                new CustomerCreated(customerId, "John Doe"),
                new CustomerMarkedAsPreferred(customerId, discountPercentage));
        given(UseProductCommandHandler.class, new ProductCreated(productId, productName, itemPrice));
        given(UseBasketCommandHandler.class, new BasketCreated(id, customerId, discountPercentage));
        //when
        when(new AddItemToBasket(id, productId, quantity));
        //then
        then(new ItemAdded(id, expectedOrderLine));*/
    }
}
