package fr.tokazio.eventsdemoapp.baskets;

import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.Address;
import fr.tokazio.eventsdemoapp.TestBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/BasketTests/CheckoutBasketTests.cs
 */
@RunWith(Parameterized.class)
public class CheckoutBasketTest extends TestBase {

    @Parameterized.Parameter
    public String street;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null}, {""}, {"    "}
        });
    }

    @Test
    public void whenTheUserCheckoutWithInvalidAddress_IShouldGetNotified() {
        Address address = street == null ? null : new Address(street);
        Guid id = Guid.generate();
      /*  given(UseBasketCommandHandler.class, new BasketCreated(id, Guid.generate(), BigDecimal.ZERO));
        whenThrows(MissingAddressException.class, new CheckoutBasket(id, address));*/
    }

    @Test
    public void whenTheUserCheckoutWithAValidAddress_IShouldProceedToTheNextStep() {
        Address address = new Address("Valid street");
        Guid id = Guid.generate();
      /*  given(UseBasketCommandHandler.class, new BasketCreated(id, Guid.generate(), BigDecimal.ZERO));
        when(new CheckoutBasket(id, address));
        then(new BasketCheckedOut(id, address));*/
    }
}
