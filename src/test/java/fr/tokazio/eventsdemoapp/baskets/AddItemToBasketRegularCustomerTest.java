package fr.tokazio.eventsdemoapp.baskets;

import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.OrderLine;
import fr.tokazio.eventsdemoapp.TestBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/BasketTests/AddItemToBasketTest.cs
 */
@RunWith(Parameterized.class)
public class AddItemToBasketRegularCustomerTest extends TestBase {

    @Parameterized.Parameter
    public String productName;
    @Parameterized.Parameter(1)
    public BigDecimal itemPrice;
    @Parameterized.Parameter(2)
    public BigDecimal quantity;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"NameA", new BigDecimal("100"), new BigDecimal("10")},
                {"NameB", new BigDecimal("200"), new BigDecimal("20")}
        });
    }

    @Test
    public void givenWeHaveABasketForARegularCustomer_WhenAddingItems_ThePriceOfTheBasketShouldNotBeDiscounted() {
        //given
        final Guid customerId = Guid.generate();
        final Guid productId = Guid.generate();
        final Guid id = Guid.generate();
        final OrderLine expectedOrderLine = new OrderLine(productId, productName, itemPrice, itemPrice, quantity);
     /*   given(UseProductCommandHandler.class, new ProductCreated(productId, productName, itemPrice));
        given(UseBasketCommandHandler.class, new BasketCreated(id, customerId, BigDecimal.ZERO));
        //when
        when(new AddItemToBasket(id, productId, quantity));
        //then
        then(new ItemAdded(id, expectedOrderLine));*/
    }


}
