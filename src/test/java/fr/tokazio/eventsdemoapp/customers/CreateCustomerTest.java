package fr.tokazio.eventsdemoapp.customers;

import fr.tokazio.events.CommandHandler;
import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.TestBase;
import fr.tokazio.eventsdemoapp.aggregates.Customer;
import fr.tokazio.eventsdemoapp.commandhandlers.UseCustomerCommandHandler;
import fr.tokazio.eventsdemoapp.commands.CreateCustomer;
import fr.tokazio.eventsdemoapp.events.CustomerCreated;
import fr.tokazio.eventsdemoapp.exceptions.CustomerAlreadyExistsException;
import org.junit.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/CustomerTests/CreateCustomerTest.cs
 */
public class CreateCustomerTest extends TestBase {

    @Inject
    @UseCustomerCommandHandler
    private CommandHandler handler;

    @Test
    public void whenCreatingTheCustomer_TheCustomerShouldBeCreatedWithTheRightName() {
        //given
        Guid id = Guid.generate();

        //when
        final Customer customer = handler.execute(new CreateCustomer(id, "Tomas"));

        //then
        assertThat(eventStore.get(id).count()).isEqualTo(1);
        assertThat(customer.getId().toString()).isEqualTo(id.toString());
        assertThat(customer.getName()).isEqualTo("Tomas");
    }

    @Test(expected = CustomerAlreadyExistsException.class)
    public void givenAUserWithIdXExists_WhenCreatingACustomerWithIdX_IShouldGetNotifiedThatTheUserAlreadyExists() {
        //given
        Guid id = Guid.generate();
        eventStore.put(new CustomerCreated(id, "Something I don't care about"));

        //when
        handler.execute(new CreateCustomer(id, "Tomas"));
    }
}
