package fr.tokazio.eventsdemoapp.customers;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import fr.tokazio.events.CommandHandler;
import fr.tokazio.events.CqAggregateCache;
import fr.tokazio.events.CqEventStore;
import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.TestBase;
import fr.tokazio.eventsdemoapp.aggregates.Customer;
import fr.tokazio.eventsdemoapp.commandhandlers.CustomerCommandHandler;
import fr.tokazio.eventsdemoapp.commandhandlers.UseCustomerCommandHandler;
import fr.tokazio.eventsdemoapp.commands.CreateCustomer;
import fr.tokazio.eventsdemoapp.commands.MarkCustomerAsPreferred;
import org.jboss.weld.junit4.WeldInitiator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/CustomerTests/MakeCustomerPreferredTest.cs
 */
@RunWith(DataProviderRunner.class)
public class MakeCustomerPreferredTest extends TestBase {

    @Rule
    public WeldInitiator weld = WeldInitiator.from(CqEventStore.class, CqAggregateCache.class, CustomerCommandHandler.class).inject(this).build();

    @Inject
    @UseCustomerCommandHandler
    private CommandHandler handler;

    @DataProvider
    public static Object[][] data() {
        return new Object[][]{
                {new BigDecimal("25")}, {new BigDecimal("50")}, {new BigDecimal("70")}
        };
    }

    @Test
    @UseDataProvider("data")
    public void givenTheUserExists_WhenMarkingCustomerAsPreferred_ThenTheCustomerShouldBePreferred(BigDecimal discount) {
        //given
        Guid id = Guid.generate();
        handler.execute(new CreateCustomer(id, "Superman"));

        //when
        final Customer customer = handler.execute(new MarkCustomerAsPreferred(id, discount));

        //then
        assertThat(eventStore.get(id).count()).isEqualTo(2);
        assertThat(customer.getId().toString()).isEqualTo(id.toString());
        assertThat(customer.getName()).isEqualTo("Superman");
        assertThat(customer.getDiscount()).isEqualByComparingTo(discount);
    }
}
