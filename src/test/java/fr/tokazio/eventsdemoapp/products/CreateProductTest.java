package fr.tokazio.eventsdemoapp.products;

import fr.tokazio.events.CommandDispatcher;
import fr.tokazio.events.Guid;
import fr.tokazio.eventsdemoapp.TestBase;
import fr.tokazio.eventsdemoapp.commandhandlers.UseProductCommandHandler;
import fr.tokazio.eventsdemoapp.commands.CreateProduct;
import fr.tokazio.eventsdemoapp.events.ProductCreated;
import fr.tokazio.eventsdemoapp.exceptions.ProductAlreadyExistsException;
import org.junit.Test;

import javax.inject.Inject;
import java.math.BigDecimal;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/ProductTests/CreateProductParameterizedTest.cs
 */
public class CreateProductTest extends TestBase {

    @Inject
    @UseProductCommandHandler
    private CommandDispatcher handler;


    @Test(expected = ProductAlreadyExistsException.class)
    public void givenProductXExists_WhenCreatingAProductWithIdX_IShouldGetNotifiedThatTheProductAlreadyExists() {
        //given
        Guid id = Guid.generate();
        eventStore.put(new ProductCreated(id, "Something I don't care about", new BigDecimal("9999")));

        //when
        handler.dispatch(new CreateProduct(id, "Sugar", new BigDecimal("999")));
    }

}
