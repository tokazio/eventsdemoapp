package fr.tokazio.eventsdemoapp.products;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import fr.tokazio.events.CommandDispatcher;
import fr.tokazio.events.Guid;
import fr.tokazio.events.infra.CqAggregateCache;
import fr.tokazio.events.infra.CqEventStore;
import fr.tokazio.eventsdemoapp.TestBase;
import fr.tokazio.eventsdemoapp.aggregates.Product;
import fr.tokazio.eventsdemoapp.commandhandlers.ProductCommandHandler;
import fr.tokazio.eventsdemoapp.commandhandlers.UseProductCommandHandler;
import fr.tokazio.eventsdemoapp.commands.CreateProduct;
import org.jboss.weld.junit4.WeldInitiator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/ProductTests/CreateProductParameterizedTest.cs
 */
@RunWith(DataProviderRunner.class)
public class CreateProductParameterizedTest extends TestBase {

    @Rule
    public WeldInitiator weld = WeldInitiator.from(CqEventStore.class, CqAggregateCache.class, ProductCommandHandler.class).inject(this).build();

    @Inject
    @UseProductCommandHandler
    private CommandDispatcher handler;

    @DataProvider
    public static Object[][] data() {
        return new Object[][]{
                {"ball", new BigDecimal("1000")}, {"train", new BigDecimal("10000")}, {"universe", new BigDecimal("999999")}
        };
    }

    @Test
    @UseDataProvider("data")
    public void whenCreatingAProduct_TheProductShouldBeCreatedWithTheCorrectPrice(String productName, BigDecimal price) {
        //given
        Guid id = Guid.generate();

        //when
        final Product product = handler.dispatch(new CreateProduct(id, "Sugar", new BigDecimal("999")));

        //then
        assertThat(eventStore.get(id).count()).isEqualTo(1);
        assertThat(product.getId().toString()).isEqualTo(id.toString());
        assertThat(product.getName()).isEqualTo("Sugar");
        assertThat(product.getPrice()).isEqualByComparingTo(new BigDecimal("999"));
    }


}
