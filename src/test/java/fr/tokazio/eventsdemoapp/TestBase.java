package fr.tokazio.eventsdemoapp;

import fr.tokazio.events.AggregateCache;
import fr.tokazio.events.EventStore;
import fr.tokazio.events.Guid;
import fr.tokazio.events.infra.WeldJUnit4Runner;
import org.junit.After;
import org.junit.runner.RunWith;

import javax.inject.Inject;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain.Tests/TestBase.cs
 */
@RunWith(WeldJUnit4Runner.class)
public abstract class TestBase {

    @Inject
    protected EventStore eventStore;
    @Inject
    protected AggregateCache aggregateCache;


    @After
    public void tearDown() {
        Guid.reset();
    }

}
