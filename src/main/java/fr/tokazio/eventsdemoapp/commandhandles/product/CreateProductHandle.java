package fr.tokazio.eventsdemoapp.commandhandles.product;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.events.exceptions.AggregateNotFoundException;
import fr.tokazio.eventsdemoapp.aggregates.Product;
import fr.tokazio.eventsdemoapp.commands.CreateProduct;
import fr.tokazio.eventsdemoapp.events.ProductCreated;
import fr.tokazio.eventsdemoapp.exceptions.ProductAlreadyExistsException;

public class CreateProductHandle extends AbstractCommandExecutor<Product, CreateProduct> {

    public CreateProductHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Product execute(CreateProduct command) {
        try {
            aggregateCache.get(Product.class, command.getAggregateId());
            throw new ProductAlreadyExistsException(command.getAggregateId());
        } catch (AggregateNotFoundException ex) {
            eventStore.put(new ProductCreated(command.getAggregateId(), command.getName(), command.getPrice()));
            return aggregateCache.get(Product.class, command.getAggregateId());
        }
    }

    @Override
    public Class<CreateProduct> acceptCommandType() {
        return CreateProduct.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
