package fr.tokazio.eventsdemoapp.commandhandles.customer;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.events.exceptions.AggregateNotFoundException;
import fr.tokazio.eventsdemoapp.aggregates.Customer;
import fr.tokazio.eventsdemoapp.commands.CreateCustomer;
import fr.tokazio.eventsdemoapp.events.CustomerCreated;
import fr.tokazio.eventsdemoapp.exceptions.CustomerAlreadyExistsException;

public class CreateCustomerHandle extends AbstractCommandExecutor<Customer, CreateCustomer> {

    public CreateCustomerHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Customer execute(CreateCustomer command) {
        try {
            aggregateCache.get(Customer.class, command.getAggregateId());
            throw new CustomerAlreadyExistsException(command.getAggregateId());
        } catch (AggregateNotFoundException ex) {
            eventStore.put(new CustomerCreated(command.getAggregateId(), command.getName()));
            return aggregateCache.get(Customer.class, command.getAggregateId());
        }
    }

    @Override
    public Class<CreateCustomer> acceptCommandType() {
        return CreateCustomer.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
