package fr.tokazio.eventsdemoapp.commandhandles.customer;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Customer;
import fr.tokazio.eventsdemoapp.commands.MarkCustomerAsPreferred;
import fr.tokazio.eventsdemoapp.events.CustomerMarkedAsPreferred;

public class MarkCustomerAsPreferredHandle extends AbstractCommandExecutor<Customer, MarkCustomerAsPreferred> {

    public MarkCustomerAsPreferredHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Customer execute(MarkCustomerAsPreferred command) {
        final Customer customer = aggregateCache.get(Customer.class, command.getAggregateId());
        eventStore.put(new CustomerMarkedAsPreferred(command.getAggregateId(), command.getDiscount()));
        return aggregateCache.get(Customer.class, command.getAggregateId());
    }

    @Override
    public Class<MarkCustomerAsPreferred> acceptCommandType() {
        return MarkCustomerAsPreferred.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
