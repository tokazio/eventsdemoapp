package fr.tokazio.eventsdemoapp.commandhandles.order;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Order;
import fr.tokazio.eventsdemoapp.commands.CancelOrder;

public class CancelOrderHandle extends AbstractCommandExecutor<Order, CancelOrder> {

    public CancelOrderHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Order execute(CancelOrder command) {
        final Order order = aggregateCache.get(Order.class, command.getAggregateId());
        order.cancel();
        return order;
    }

    @Override
    public Class<CancelOrder> acceptCommandType() {
        return CancelOrder.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
