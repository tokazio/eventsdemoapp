package fr.tokazio.eventsdemoapp.commandhandles.order;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Order;
import fr.tokazio.eventsdemoapp.commands.ApproveOrder;

public class ApproveOrderHandle extends AbstractCommandExecutor<Order, ApproveOrder> {

    public ApproveOrderHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Order execute(ApproveOrder command) {
        final Order order = aggregateCache.get(Order.class, command.getAggregateId());
        order.approve();
        return order;
    }

    @Override
    public Class<ApproveOrder> acceptCommandType() {
        return ApproveOrder.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
