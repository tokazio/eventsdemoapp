package fr.tokazio.eventsdemoapp.commandhandles.order;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Order;
import fr.tokazio.eventsdemoapp.commands.StartShippingProcess;

public class StartShippingProcessHandle extends AbstractCommandExecutor<Order, StartShippingProcess> {

    public StartShippingProcessHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Order execute(StartShippingProcess command) {
        final Order order = aggregateCache.get(Order.class, command.getAggregateId());
        order.startShippingProcess();
        return order;
    }

    @Override
    public Class<StartShippingProcess> acceptCommandType() {
        return StartShippingProcess.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
