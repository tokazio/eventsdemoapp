package fr.tokazio.eventsdemoapp.commandhandles.order;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Order;
import fr.tokazio.eventsdemoapp.commands.ShipOrder;

public class ShipOrderHandle extends AbstractCommandExecutor<Order, ShipOrder> {

    public ShipOrderHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Order execute(ShipOrder command) {
        final Order order = aggregateCache.get(Order.class, command.getAggregateId());
        order.shipOrder();
        return order;
    }

    @Override
    public Class<ShipOrder> acceptCommandType() {
        return ShipOrder.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
