package fr.tokazio.eventsdemoapp.commandhandles.basket;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Basket;
import fr.tokazio.eventsdemoapp.aggregates.Product;
import fr.tokazio.eventsdemoapp.commands.AddItemToBasket;

public class AddItemToBasketHandle extends AbstractCommandExecutor<Basket, AddItemToBasket> {

    public AddItemToBasketHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Basket execute(AddItemToBasket command) {
        final Basket basket = aggregateCache.get(Basket.class, command.getAggregateId());
        final Product product = aggregateCache.get(Product.class, command.getProductId());
        basket.addItem(product, command.getQuantity());
        return basket;
    }

    @Override
    public Class<AddItemToBasket> acceptCommandType() {
        return AddItemToBasket.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
