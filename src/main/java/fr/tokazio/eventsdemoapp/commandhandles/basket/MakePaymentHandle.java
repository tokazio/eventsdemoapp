package fr.tokazio.eventsdemoapp.commandhandles.basket;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Basket;
import fr.tokazio.eventsdemoapp.aggregates.Order;
import fr.tokazio.eventsdemoapp.commands.MakePayment;

public class MakePaymentHandle extends AbstractCommandExecutor<Order, MakePayment> {

    public MakePaymentHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Order execute(MakePayment command) {
        final Basket basket = aggregateCache.get(Basket.class, command.getAggregateId());
        return basket.makePayment(command.getPayment());
    }

    @Override
    public Class<MakePayment> acceptCommandType() {
        return MakePayment.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
