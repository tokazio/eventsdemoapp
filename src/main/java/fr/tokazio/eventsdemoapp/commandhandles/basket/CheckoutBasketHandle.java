package fr.tokazio.eventsdemoapp.commandhandles.basket;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Basket;
import fr.tokazio.eventsdemoapp.commands.CheckoutBasket;

public class CheckoutBasketHandle extends AbstractCommandExecutor<Basket, CheckoutBasket> {

    public CheckoutBasketHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Basket execute(CheckoutBasket command) {
        final Basket basket = aggregateCache.get(Basket.class, command.getAggregateId());
        basket.checkout(command.getShippingAddress());
        return basket;
    }

    @Override
    public Class<CheckoutBasket> acceptCommandType() {
        return CheckoutBasket.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
