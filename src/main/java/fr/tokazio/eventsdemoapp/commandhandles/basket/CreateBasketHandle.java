package fr.tokazio.eventsdemoapp.commandhandles.basket;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.events.exceptions.AggregateNotFoundException;
import fr.tokazio.eventsdemoapp.aggregates.Basket;
import fr.tokazio.eventsdemoapp.commands.CreateBasket;
import fr.tokazio.eventsdemoapp.events.BasketCreated;
import fr.tokazio.eventsdemoapp.exceptions.BasketAlreadExistsException;

import java.math.BigDecimal;

public class CreateBasketHandle extends AbstractCommandExecutor<Basket, CreateBasket> {

    public CreateBasketHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Basket execute(CreateBasket command) {
        try {
            aggregateCache.get(Basket.class, command.getAggregateId());
            throw new BasketAlreadExistsException(command.getAggregateId());
        } catch (AggregateNotFoundException ex) {
            eventStore.put(new BasketCreated(command.getAggregateId(), command.getCustomerId(), BigDecimal.ZERO));
            return aggregateCache.get(Basket.class, command.getAggregateId());
        }
    }

    @Override
    public Class<CreateBasket> acceptCommandType() {
        return CreateBasket.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
