package fr.tokazio.eventsdemoapp.commandhandles.basket;

import fr.tokazio.events.AbstractCommandExecutor;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.aggregates.Basket;
import fr.tokazio.eventsdemoapp.commands.ProceedToCheckout;

public class ProceedToCheckoutHandle extends AbstractCommandExecutor<Basket, ProceedToCheckout> {

    public ProceedToCheckoutHandle(EventStore eventStore, AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public Basket execute(ProceedToCheckout command) {
        final Basket basket = aggregateCache.get(Basket.class, command.getAggregateId());
        basket.proceedToCheckout();
        return basket;
    }

    @Override
    public Class<ProceedToCheckout> acceptCommandType() {
        return ProceedToCheckout.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}
