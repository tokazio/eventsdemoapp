package fr.tokazio.eventsdemoapp;

import fr.tokazio.events.annotations.ImAValueObject;

import java.io.Serializable;
import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Types.fs
 */
@ImAValueObject
public class Address implements Serializable {

    private final String street;

    public Address(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(getStreet(), address.getStreet());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStreet());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Address{").append("street='").append(street).append('\'').append('}').toString();
    }
}
