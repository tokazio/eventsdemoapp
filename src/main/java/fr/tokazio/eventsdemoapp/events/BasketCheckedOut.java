package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;
import fr.tokazio.eventsdemoapp.Address;

import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class BasketCheckedOut extends AbstractEvent {

    private final Address shippingAddress;

    public BasketCheckedOut(Guid id, Address shippingAddress) {
        super(id, 1);
        this.shippingAddress = shippingAddress;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasketCheckedOut)) return false;
        BasketCheckedOut that = (BasketCheckedOut) o;
        return Objects.equals(getAggregateId(), that.getAggregateId()) &&
                Objects.equals(getShippingAddress(), that.getShippingAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId(), getShippingAddress());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("BasketCheckedOut{").append("basketId=").append(getAggregateId()).append(", shippingAddress=").append(shippingAddress).append('}').toString();
    }
}
