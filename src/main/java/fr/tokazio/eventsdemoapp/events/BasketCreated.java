package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class BasketCreated extends AbstractEvent {

    private Guid customerId;
    private BigDecimal discount;

    public BasketCreated(Guid id, Guid customerId, BigDecimal discount) {
        super(id, 1);
        this.customerId = customerId;
        setDiscount(discount);
    }


    public Guid getCustomerId() {
        return customerId;
    }

    public BigDecimal getDiscount() {
        return discount != null ? discount : BigDecimal.ZERO;
    }

    private void setDiscount(BigDecimal discount) {
        this.discount = discount != null ? discount : BigDecimal.ZERO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasketCreated)) return false;
        BasketCreated that = (BasketCreated) o;
        return Objects.equals(getAggregateId(), that.getAggregateId()) &&
                Objects.equals(getCustomerId(), that.getCustomerId()) &&
                Objects.equals(getDiscount(), that.getDiscount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId(), getCustomerId(), getDiscount());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("BasketCreated{").append("id=").append(getAggregateId()).append(", customerId=").append(customerId).append(", discount=").append(discount).append('}').toString();
    }
}
