package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;

import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class OrderApproved extends AbstractEvent {


    public OrderApproved(Guid id) {
        super(id, 1);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderApproved)) return false;
        OrderApproved that = (OrderApproved) o;
        return Objects.equals(getAggregateId(), that.getAggregateId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("OrderApproved{").append("id=").append(getAggregateId()).append('}').toString();
    }
}
