package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;
import fr.tokazio.eventsdemoapp.OrderLine;

import java.util.List;
import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class OrderCreated extends AbstractEvent {


    private Guid basketId;

    private List<OrderLine> orderLines;

    public OrderCreated(Guid orderId, Guid basketId, List<OrderLine> orderLines) {
        super(orderId, 1);
        this.basketId = basketId;
        this.orderLines = orderLines;
    }

    public Guid getBasketId() {
        return basketId;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderCreated)) return false;
        OrderCreated that = (OrderCreated) o;
        return Objects.equals(getAggregateId(), that.getAggregateId()) &&
                Objects.equals(getBasketId(), that.getBasketId()) &&
                Objects.equals(getOrderLines(), that.getOrderLines());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId(), getBasketId());//, getOrderLines());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("OrderCreated{").append("orderId=").append(getAggregateId()).append(", basketId=").append(basketId).append(", orderLines=").append(orderLines).append('}').toString();
    }
}
