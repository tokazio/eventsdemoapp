package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;

import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class OrderCancelled extends AbstractEvent {


    public OrderCancelled(Guid id) {
        super(id, 1);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderCancelled)) return false;
        OrderCancelled that = (OrderCancelled) o;
        return Objects.equals(getAggregateId(), that.getAggregateId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId());
    }
}
