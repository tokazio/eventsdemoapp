package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class ProductCreated extends AbstractEvent {

    private String name;
    private BigDecimal price;

    public ProductCreated(Guid id, String name, BigDecimal price) {
        super(id, 0);
        this.name = name;
        setPrice(price);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    private void setPrice(BigDecimal price) {
        this.price = price != null ? price : BigDecimal.ZERO;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductCreated)) return false;
        ProductCreated that = (ProductCreated) o;
        return Objects.equals(getAggregateId(), that.getAggregateId()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getPrice(), that.getPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId(), getName(), getPrice());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ProductCreated{").append("id=").append(getAggregateId()).append(", name='").append(name).append('\'').append(", price=").append(price).append('}').toString();
    }
}
