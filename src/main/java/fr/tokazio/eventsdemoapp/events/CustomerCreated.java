package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;

import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class CustomerCreated extends AbstractEvent {

    private String name;

    public CustomerCreated(Guid id, String name) {
        super(id, 0);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerCreated)) return false;
        CustomerCreated that = (CustomerCreated) o;
        return Objects.equals(getAggregateId(), that.getAggregateId()) &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId(), getName());
    }

    @Override
    public String toString() {
        return "CustomerCreated{" + "id=" + getAggregateId() + ", name='" + name + '\'' + '}';
    }
}
