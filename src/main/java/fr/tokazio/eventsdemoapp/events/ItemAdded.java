package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;
import fr.tokazio.eventsdemoapp.OrderLine;

import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class ItemAdded extends AbstractEvent {

    private OrderLine orderLine;

    public ItemAdded(Guid id, OrderLine orderLine) {
        super(id, 1);
        this.orderLine = orderLine;
    }

    public OrderLine getOrderLine() {
        return orderLine;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemAdded)) return false;
        ItemAdded itemAdded = (ItemAdded) o;
        return Objects.equals(getAggregateId(), itemAdded.getAggregateId()) &&
                Objects.equals(getOrderLine(), itemAdded.getOrderLine());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId(), getOrderLine());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ItemAdded{").append("id=").append(getAggregateId()).append(", orderLine=").append(orderLine).append('}').toString();
    }
}
