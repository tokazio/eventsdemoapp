package fr.tokazio.eventsdemoapp.events;


import fr.tokazio.events.AbstractEvent;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnEvent;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Events.fs
 */
@ImAnEvent
public class CustomerMarkedAsPreferred extends AbstractEvent {


    private BigDecimal discount;

    public CustomerMarkedAsPreferred(Guid id, BigDecimal discount) {
        super(id, 1);
        setDiscount(discount);
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    private void setDiscount(BigDecimal discount) {
        this.discount = discount != null ? discount : BigDecimal.ZERO;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerMarkedAsPreferred)) return false;
        CustomerMarkedAsPreferred that = (CustomerMarkedAsPreferred) o;
        return Objects.equals(getAggregateId(), that.getAggregateId()) &&
                Objects.equals(getDiscount(), that.getDiscount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateId(), getDiscount());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("CustomerMarkedAsPreferred{").append("id=").append(getAggregateId()).append(", discount=").append(getDiscount()).append('}').toString();
    }
}
