package fr.tokazio.eventsdemoapp.commandhandlers;

import fr.tokazio.events.AbstractCommandDispatcher;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.commandhandles.order.ApproveOrderHandle;
import fr.tokazio.eventsdemoapp.commandhandles.order.CancelOrderHandle;
import fr.tokazio.eventsdemoapp.commandhandles.order.ShipOrderHandle;
import fr.tokazio.eventsdemoapp.commandhandles.order.StartShippingProcessHandle;

import javax.inject.Inject;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/CommandHandlers/OrderHandler.cs
 */
@UseOrderCommandHandler
public class OrderCommandHandler extends AbstractCommandDispatcher {

    @Inject
    public OrderCommandHandler(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
        register(new ApproveOrderHandle(eventStore, aggregateCache));
        register(new StartShippingProcessHandle(eventStore, aggregateCache));
        register(new CancelOrderHandle(eventStore, aggregateCache));
        register(new ShipOrderHandle(eventStore, aggregateCache));
    }


}
