package fr.tokazio.eventsdemoapp.commandhandlers;

import fr.tokazio.events.AbstractCommandDispatcher;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.commandhandles.product.CreateProductHandle;

import javax.inject.Inject;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/CommandHandlers/ProductCommandHandler.cs
 */
@UseProductCommandHandler
public class ProductCommandHandler extends AbstractCommandDispatcher {

    @Inject
    public ProductCommandHandler(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
        register(new CreateProductHandle(eventStore, aggregateCache));
    }
}
