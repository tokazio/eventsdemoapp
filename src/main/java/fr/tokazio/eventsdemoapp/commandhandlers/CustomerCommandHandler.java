package fr.tokazio.eventsdemoapp.commandhandlers;


import fr.tokazio.events.AbstractCommandDispatcher;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.commandhandles.customer.CreateCustomerHandle;
import fr.tokazio.eventsdemoapp.commandhandles.customer.MarkCustomerAsPreferredHandle;

import javax.inject.Inject;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/CommandHandlers/CustomerCommandHandler.cs
 */
@UseCustomerCommandHandler
public class CustomerCommandHandler extends AbstractCommandDispatcher {

    @Inject
    public CustomerCommandHandler(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
        register(new CreateCustomerHandle(eventStore, aggregateCache));
        register(new MarkCustomerAsPreferredHandle(eventStore, aggregateCache));
    }
}
