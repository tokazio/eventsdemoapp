package fr.tokazio.eventsdemoapp.commandhandlers;

import fr.tokazio.events.AbstractCommandDispatcher;
import fr.tokazio.events.AggregateManager;
import fr.tokazio.events.EventStore;
import fr.tokazio.eventsdemoapp.commandhandles.basket.*;

import javax.inject.Inject;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/CommandHandlers/BasketCommandHandler.cs
 */
@UseBasketCommandHandler
public class BasketCommandHandler extends AbstractCommandDispatcher {

    @Inject
    public BasketCommandHandler(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
        register(new CreateBasketHandle(eventStore, aggregateCache));
        register(new AddItemToBasketHandle(eventStore, aggregateCache));
        register(new ProceedToCheckoutHandle(eventStore, aggregateCache));
        register(new CheckoutBasketHandle(eventStore, aggregateCache));
        register(new MakePaymentHandle(eventStore, aggregateCache));
    }
}
