package fr.tokazio.eventsdemoapp.commands;


import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;
import fr.tokazio.eventsdemoapp.Address;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class CheckoutBasket extends AbstractCommand {

    private final Address shippingAddress;

    public CheckoutBasket(Guid id, Address shippingAddress) {
        super(id, 1);
        this.shippingAddress = shippingAddress;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }
}
