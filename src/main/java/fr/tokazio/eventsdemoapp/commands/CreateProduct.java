package fr.tokazio.eventsdemoapp.commands;


import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

import java.math.BigDecimal;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class CreateProduct extends AbstractCommand {

    private final String name;

    private final BigDecimal price;


    public CreateProduct(Guid id, String name, BigDecimal price) {
        super(id, 0);
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
