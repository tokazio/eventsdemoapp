package fr.tokazio.eventsdemoapp.commands;


import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class ProceedToCheckout extends AbstractCommand {


    public ProceedToCheckout(Guid idAggregat, long aggregateVersion) {
        super(idAggregat, aggregateVersion);
    }
}
