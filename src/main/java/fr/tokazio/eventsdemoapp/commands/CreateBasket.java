package fr.tokazio.eventsdemoapp.commands;


import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class CreateBasket extends AbstractCommand {

    private final Guid customerId;

    public CreateBasket(Guid id, Guid customerId) {
        super(id, 0);
        this.customerId = customerId;
    }

    public Guid getCustomerId() {
        return customerId;
    }

}
