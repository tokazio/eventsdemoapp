package fr.tokazio.eventsdemoapp.commands;

import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class ApproveOrder extends AbstractCommand {


    public ApproveOrder(Guid idAggregat, long aggregateVersion) {
        super(idAggregat, aggregateVersion);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApproveOrder{");
        sb.append("orderId=").append(getAggregateId());
        sb.append('}');
        return sb.toString();
    }
}
