package fr.tokazio.eventsdemoapp.commands;

import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

import java.math.BigDecimal;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class AddItemToBasket extends AbstractCommand {

    private final Guid productId;

    private final BigDecimal quantity;


    public AddItemToBasket(Guid id, Guid productId, BigDecimal quantity) {
        super(id, 1);
        this.productId = productId;
        this.quantity = quantity;
    }

    public Guid getProductId() {
        return productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

}
