package fr.tokazio.eventsdemoapp.commands;


import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class StartShippingProcess extends AbstractCommand {


    public StartShippingProcess(Guid idAggregat, long aggregateVersion) {
        super(idAggregat, aggregateVersion);
    }
}
