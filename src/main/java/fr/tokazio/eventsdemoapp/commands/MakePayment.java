package fr.tokazio.eventsdemoapp.commands;


import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

import java.math.BigDecimal;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class MakePayment extends AbstractCommand {

    private final BigDecimal payment;


    public MakePayment(Guid id, BigDecimal payment) {
        super(id, 1);
        this.payment = payment;
    }

    public BigDecimal getPayment() {
        return payment;
    }
}
