package fr.tokazio.eventsdemoapp.commands;


import fr.tokazio.events.AbstractCommand;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImACommand;

import java.math.BigDecimal;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Commands.fs
 */
@ImACommand
public class MarkCustomerAsPreferred extends AbstractCommand {

    private final BigDecimal discount;

    public MarkCustomerAsPreferred(Guid id, BigDecimal discount) {
        super(id, 1);
        this.discount = discount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

}
