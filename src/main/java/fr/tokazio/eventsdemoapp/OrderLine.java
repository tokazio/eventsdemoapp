package fr.tokazio.eventsdemoapp;

import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAValueObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Contracts/Types.fs
 */
@ImAValueObject
public class OrderLine implements Serializable {

    private Guid id;

    private String productName;

    private BigDecimal originalPrice;

    private BigDecimal discountedPrice;

    private BigDecimal quantity;

    public OrderLine(Guid id, String productName, BigDecimal originalPrice, BigDecimal discountedPrice, BigDecimal quantity) {
        this.id = id;
        this.productName = productName;
        this.originalPrice = originalPrice;
        this.discountedPrice = discountedPrice;
        this.quantity = quantity;
    }

    public Guid getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public BigDecimal getDiscountedPrice() {
        return discountedPrice;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderLine)) return false;
        OrderLine orderLine = (OrderLine) o;
        return (getId() == null || Objects.equals(getId(), orderLine.getId())) &&
                Objects.equals(getProductName(), orderLine.getProductName()) &&
                Objects.equals(getOriginalPrice(), orderLine.getOriginalPrice()) &&
                Objects.equals(getDiscountedPrice(), orderLine.getDiscountedPrice()) &&
                Objects.equals(getQuantity(), orderLine.getQuantity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProductName(), getOriginalPrice(), getDiscountedPrice(), getQuantity());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("OrderLine{").append("id=").append(id).append(", productName='").append(productName).append('\'').append(", originalPrice=").append(originalPrice).append(", discountedPrice=").append(discountedPrice).append(", quantity=").append(quantity).append('}').toString();
    }
}
