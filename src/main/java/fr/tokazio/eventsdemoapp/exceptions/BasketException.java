package fr.tokazio.eventsdemoapp.exceptions;

public class BasketException extends RuntimeException {

    public BasketException(String s) {
        super(s);
    }
}
