package fr.tokazio.eventsdemoapp.exceptions;

public class ApplicationException extends RuntimeException {

    public ApplicationException(String s) {
        super(s);
    }
}
