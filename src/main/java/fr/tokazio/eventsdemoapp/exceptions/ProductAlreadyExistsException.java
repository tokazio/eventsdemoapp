package fr.tokazio.eventsdemoapp.exceptions;

import fr.tokazio.events.Guid;

public class ProductAlreadyExistsException extends RuntimeException {
    public ProductAlreadyExistsException(Guid id) {
        super("UseProductCommandHandler " + id.toString() + " alreadyExists");
    }
}
