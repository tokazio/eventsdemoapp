package fr.tokazio.eventsdemoapp.exceptions;

public class UnexpectedPaymentException extends RuntimeException {

    public UnexpectedPaymentException(String s) {
        super(s);
    }
}
