package fr.tokazio.eventsdemoapp.exceptions;


import fr.tokazio.events.Guid;

public class BasketAlreadExistsException extends RuntimeException {
    public BasketAlreadExistsException(Guid id) {
        super(id.toString());
    }
}
