package fr.tokazio.eventsdemoapp.exceptions;

import fr.tokazio.events.Guid;

public class CustomerAlreadyExistsException extends RuntimeException {

    public CustomerAlreadyExistsException(Guid id) {
        super("UseCustomerCommandHandler " + id.toString() + " already exists");
    }
}
