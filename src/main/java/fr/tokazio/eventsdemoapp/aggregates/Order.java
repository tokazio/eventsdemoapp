package fr.tokazio.eventsdemoapp.aggregates;

import fr.tokazio.events.AbstractAggregate;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnAggregate;
import fr.tokazio.eventsdemoapp.exceptions.InvalidOrderState;
import fr.tokazio.eventsdemoapp.exceptions.OrderCancelledException;
import fr.tokazio.eventsdemoapp.exceptions.ShippingStartedException;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/Aggregates/UseOrderCommandHandler.cs
 */
@ImAnAggregate
public class Order extends AbstractAggregate {

    private Guid id;
    private OrderState orderState;

    /*
    public UseOrderCommandHandler(Guid basketId, List<OrderLine> orderLines) {
        this();
        id = Guid.generate();
        raiseEvent(new OrderCreated(id, basketId, orderLines));
        BigDecimal totalPrice = orderLines.stream().map(OrderLine::getDiscountedPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (totalPrice.compareTo(new BigDecimal("100000")) > 0) {
            raiseEvent(new NeedsApproval(id));
        } else {
            raiseEvent(new OrderApproved(id));
        }
    }

    public UseOrderCommandHandler() {
        registerTransition(OrderCreated.class, event -> {
            setId(event.getAggregateId());
            orderState = OrderState.Created;
        });
        registerTransition(ShippingProcessStarted.class, event -> orderState = OrderState.ShippingProcessStarted);
        registerTransition(OrderCancelled.class, event -> orderState = OrderState.Cancelled);
    }
    */

    public void approve() {
        // raiseEvent(new OrderApproved(getId()));
    }

    public void startShippingProcess() {
        if (orderState == OrderState.Cancelled)
            throw new OrderCancelledException();

        // raiseEvent(new ShippingProcessStarted(getId()));
    }

    public void cancel() {
        if (orderState == OrderState.Created) {
            //    raiseEvent(new OrderCancelled(getId()));
        } else {
            throw new ShippingStartedException();
        }
    }

    public void shipOrder() {
        if (orderState != OrderState.ShippingProcessStarted)
            throw new InvalidOrderState();
        // raiseEvent(new OrderShipped(getId()));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UseOrderCommandHandler{");
        sb.append("id=").append(id);
        sb.append(", orderState=").append(orderState);
        sb.append('}');
        return sb.toString();
    }

    private enum OrderState {
        ShippingProcessStarted,
        Created,
        Cancelled
    }
}
