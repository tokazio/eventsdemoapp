package fr.tokazio.eventsdemoapp.aggregates;


import fr.tokazio.events.AbstractAggregate;
import fr.tokazio.events.Guid;
import fr.tokazio.events.annotations.ImAnAggregate;
import fr.tokazio.eventsdemoapp.Address;
import fr.tokazio.eventsdemoapp.OrderLine;
import fr.tokazio.eventsdemoapp.events.BasketCreated;
import fr.tokazio.eventsdemoapp.events.ItemAdded;
import fr.tokazio.eventsdemoapp.exceptions.BasketException;
import fr.tokazio.eventsdemoapp.exceptions.MissingAddressException;
import fr.tokazio.eventsdemoapp.exceptions.UnexpectedPaymentException;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/Aggregates/UseBasketCommandHandler.cs
 */
@ImAnAggregate
public class Basket extends AbstractAggregate {

    private final List<OrderLine> orderLines = new ArrayList<>();
    private BigDecimal discount = BigDecimal.ZERO;
    private Guid customerId;

    public Basket apply(BasketCreated event) {
        this.id = event.getAggregateId();
        this.customerId = event.getCustomerId();
        this.discount = event.getDiscount() != null ? event.getDiscount() : BigDecimal.ZERO;
        return this;
    }

    public Basket apply(ItemAdded event) {
        orderLines.add(event.getOrderLine());
        return this;
    }

    //to AddItemToBasketHandle
    public void addItem(final Product product, BigDecimal quantity) {
        if (product == null) {
            throw new BasketException("Can't add null product to basket");
        }
        if (quantity == null) {
            quantity = BigDecimal.ZERO;
        }
        final BigDecimal discount = product.getPrice().multiply(this.discount.divide(new BigDecimal("100"), MathContext.DECIMAL32));
        final BigDecimal discountedPrice = product.getPrice().subtract(discount, MathContext.DECIMAL32);
        final OrderLine orderLine = new OrderLine(product.getId(), product.getName(), product.getPrice(), discountedPrice, quantity);
        //   raiseEvent(new ItemAdded(getId(), orderLine));
    }

    //To proceedToCheckoutHandle
    public void proceedToCheckout() {
        //   raiseEvent(new CustomerIsCheckingOutBasket(getId()));
    }

    //to checkoutBasketHandle
    public void checkout(Address shippingAddress) {
        if (shippingAddress == null || shippingAddress.getStreet().isEmpty())
            throw new MissingAddressException();
        //   raiseEvent(new BasketCheckedOut(getId(), shippingAddress));
    }

    //to makePaymentHandle
    public Order makePayment(BigDecimal payment) {
        final BigDecimal expectedPayment = orderLines.stream().map(orderLine -> orderLine.getDiscountedPrice().multiply(orderLine.getQuantity())).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (expectedPayment.compareTo(payment) != 0)
            throw new UnexpectedPaymentException("Got " + payment + " instead of " + expectedPayment);
        return null;//new UseOrderCommandHandler(getId(), orderLines);
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public Guid getCustomerId() {
        return customerId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UseBasketCommandHandler{");
        sb.append("orderLines=").append(orderLines);
        sb.append(", discount=").append(discount);
        sb.append('}');
        return sb.toString();
    }
}
