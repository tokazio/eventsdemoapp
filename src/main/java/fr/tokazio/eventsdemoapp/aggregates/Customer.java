package fr.tokazio.eventsdemoapp.aggregates;

import fr.tokazio.events.AbstractAggregate;
import fr.tokazio.events.annotations.ImAnAggregate;
import fr.tokazio.eventsdemoapp.events.CustomerCreated;
import fr.tokazio.eventsdemoapp.events.CustomerMarkedAsPreferred;

import java.math.BigDecimal;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/Aggregates/UseCustomerCommandHandler.cs
 */
@ImAnAggregate
public class Customer extends AbstractAggregate {

    private BigDecimal discount;
    private String name;

    public Customer apply(CustomerCreated event) {
        this.id = event.getAggregateId();
        this.name = event.getName();
        return this;
    }

    public Customer apply(CustomerMarkedAsPreferred event) {
        this.discount = event.getDiscount();
        return this;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UseCustomerCommandHandler{");
        sb.append("discount=").append(discount);
        sb.append('}');
        return sb.toString();
    }
}
