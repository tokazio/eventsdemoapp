package fr.tokazio.eventsdemoapp.aggregates;

import fr.tokazio.events.AbstractAggregate;
import fr.tokazio.events.annotations.ImAnAggregate;
import fr.tokazio.eventsdemoapp.events.ProductCreated;

import java.math.BigDecimal;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Domain/Aggregates/UseProductCommandHandler.cs
 */
@ImAnAggregate
public class Product extends AbstractAggregate {

    private String name;
    private BigDecimal price;

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price != null ? price : BigDecimal.ZERO;
    }

    public Product apply(ProductCreated event) {
        this.id = event.getAggregateId();
        this.name = event.getName();
        this.price = event.getPrice();
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UseProductCommandHandler{");
        sb.append("name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
